# pip install 'stable-baselines3[extra]' pyglet
import gym
from gym import Env
from gym.spaces import Discrete, Box, Dict, Tuple, MultiBinary, MultiDiscrete 

# import helpers
import numpy as np
import random
import math
import time


# --------------------------- BEGIN CUSTOM ENV --------------------------------------

SCALE = 50.0

DIM_ARRAY = 5  # deve ser ímpar (por questão de simetria)! Se mudar, alterar o tamanho da janela na linha 151
GAME_LENGTH_TOTAL = math.ceil(pow(DIM_ARRAY, 2) * 0.75)
POS_PLAYER = int((pow(DIM_ARRAY, 2) - 1) / 2)

VIEWPORT_W = 800
VIEWPORT_H = 800

random.seed()
POS_INIMIGO_X = random.choice([0, DIM_ARRAY - 1, (pow(DIM_ARRAY, 2) - DIM_ARRAY), (pow(DIM_ARRAY, 2) - 1)])  # cantos do quadrado
POS_INIMIGO_Y = random.choice([0, DIM_ARRAY - 1, (pow(DIM_ARRAY, 2) - DIM_ARRAY), (pow(DIM_ARRAY, 2) - 1)])  # cantos do quadrado

class DodgeEnv(Env):
    def __init__(self):
        self.viewer = None
        # atribui o estado inicial do player (sua posição)
        self.state = POS_PLAYER
        
        # inimigo
        self.where = random.choice([0, 1, 2, 3])
        '''
        where == 0: top
        where == 1: bottom
        where == 2: left
        where == 3: right
        '''
        if (self.where == 0):
            self.pos_inimigo = random.choice(np.arange(0,DIM_ARRAY))
        if (self.where == 1):
            self.pos_inimigo = random.choice(np.arange((pow(DIM_ARRAY, 2)-DIM_ARRAY),pow(DIM_ARRAY, 2)))
        if (self.where == 2):
            self.pos_inimigo = random.choice(np.arange(0,(pow(DIM_ARRAY, 2)-(DIM_ARRAY-1)),DIM_ARRAY))
        if (self.where == 3):
            self.pos_inimigo = random.choice(np.arange((DIM_ARRAY-1),pow(DIM_ARRAY, 2),DIM_ARRAY))
        
        # movimentação (8): leste, nordeste, norte, noroeste, oeste, sudoeste, sul e sudeste (respectivamente).
        self.action_space = Discrete(8)
        
        # estacionamento
        high = np.array(
            [
                pow(DIM_ARRAY, 2),
                pow(DIM_ARRAY, 2),
            ],
            dtype=np.int32,
        )
        self.observation_space = Box(-high, high, dtype=np.int32)
        
        # tempo total do jogo (em passos)
        self.game_length = GAME_LENGTH_TOTAL
        
    def step(self, action):
        # array para parede norte
        arr_norte = []
        for i in range(0, DIM_ARRAY):
            arr_norte.append(i)
        # array para parede sul
        arr_sul = []
        for i in range(0, DIM_ARRAY):
            arr_sul.append((pow(DIM_ARRAY, 2) - (DIM_ARRAY - i)))
        # array para parede leste
        arr_leste = []
        for i in range(0, DIM_ARRAY):
            arr_leste.append((((i + 1) * DIM_ARRAY) - 1))
        # array para parede oeste
        arr_oeste = []
        for i in range(0, DIM_ARRAY):
            arr_oeste.append((((i + 1) * DIM_ARRAY) - DIM_ARRAY))
        
        # movimenta o player validando a tentativa de movimentação
        ## leste
        if action == 0 and self.state[0] not in arr_leste:
            self.state[0] = self.state[0] + 1
        ## nordeste
        if action == 1 and self.state[0] not in arr_norte and self.state[0] not in arr_leste:
            self.state[0] = self.state[0] - (DIM_ARRAY - 1)
        ## norte
        if action == 2 and self.state[0] not in arr_norte:
            self.state[0] = self.state[0] - DIM_ARRAY
        ## noroeste
        if action == 3 and self.state[0] not in arr_norte and self.state[0] not in arr_oeste:
            self.state[0] = self.state[0] - (DIM_ARRAY + 1)
        ## oeste
        if action == 4 and self.state[0] not in arr_oeste:
            self.state[0] = self.state[0] - 1
        ## sudoeste
        if action == 5 and self.state[0] not in arr_sul and self.state[0] not in arr_oeste:
            self.state[0] = self.state[0] + (DIM_ARRAY - 1)
        ## sul
        if action == 6 and self.state[0] not in arr_sul:
            self.state[0] = self.state[0] + DIM_ARRAY
        ## sudeste
        if action == 7 and self.state[0] not in arr_sul and self.state[0] not in arr_leste:
            self.state[0] = self.state[0] + (DIM_ARRAY + 1)
        
        # movimenta inimigo e reseta sua posição se chegou no final
        if self.where == 0: # top
            if self.pos_inimigo not in arr_sul:
                self.pos_inimigo += DIM_ARRAY
            else:
                self.where = random.choice([0, 1, 2, 3])
                if (self.where == 0):
                    self.pos_inimigo = random.choice(np.arange(0,DIM_ARRAY))
                if (self.where == 1):
                    self.pos_inimigo = random.choice(np.arange((pow(DIM_ARRAY, 2)-DIM_ARRAY),pow(DIM_ARRAY, 2)))
                if (self.where == 2):
                    self.pos_inimigo = random.choice(np.arange(0,(pow(DIM_ARRAY, 2)-(DIM_ARRAY-1)),DIM_ARRAY))
                if (self.where == 3):
                    self.pos_inimigo = random.choice(np.arange((DIM_ARRAY-1),pow(DIM_ARRAY, 2),DIM_ARRAY))
        if self.where == 1: # bottom
            if self.pos_inimigo not in arr_norte:
                self.pos_inimigo -= DIM_ARRAY
            else:
                self.where = random.choice([0, 1, 2, 3])
                if (self.where == 0):
                    self.pos_inimigo = random.choice(np.arange(0,DIM_ARRAY))
                if (self.where == 1):
                    self.pos_inimigo = random.choice(np.arange((pow(DIM_ARRAY, 2)-DIM_ARRAY),pow(DIM_ARRAY, 2)))
                if (self.where == 2):
                    self.pos_inimigo = random.choice(np.arange(0,(pow(DIM_ARRAY, 2)-(DIM_ARRAY-1)),DIM_ARRAY))
                if (self.where == 3):
                    self.pos_inimigo = random.choice(np.arange((DIM_ARRAY-1),pow(DIM_ARRAY, 2),DIM_ARRAY))
        if self.where == 2: # left
            if self.pos_inimigo not in arr_leste:
                self.pos_inimigo += 1
            else:
                self.where = random.choice([0, 1, 2, 3])
                if (self.where == 0):
                    self.pos_inimigo = random.choice(np.arange(0,DIM_ARRAY))
                if (self.where == 1):
                    self.pos_inimigo = random.choice(np.arange((pow(DIM_ARRAY, 2)-DIM_ARRAY),pow(DIM_ARRAY, 2)))
                if (self.where == 2):
                    self.pos_inimigo = random.choice(np.arange(0,(pow(DIM_ARRAY, 2)-(DIM_ARRAY-1)),DIM_ARRAY))
                if (self.where == 3):
                    self.pos_inimigo = random.choice(np.arange((DIM_ARRAY-1),pow(DIM_ARRAY, 2),DIM_ARRAY))
        if self.where == 3: # right
            if self.pos_inimigo not in arr_oeste:
                self.pos_inimigo -= 1
            else:
                self.where = random.choice([0, 1, 2, 3])
                if (self.where == 0):
                    self.pos_inimigo = random.choice(np.arange(0,DIM_ARRAY))
                if (self.where == 1):
                    self.pos_inimigo = random.choice(np.arange((pow(DIM_ARRAY, 2)-DIM_ARRAY),pow(DIM_ARRAY, 2)))
                if (self.where == 2):
                    self.pos_inimigo = random.choice(np.arange(0,(pow(DIM_ARRAY, 2)-(DIM_ARRAY-1)),DIM_ARRAY))
                if (self.where == 3):
                    self.pos_inimigo = random.choice(np.arange((DIM_ARRAY-1),pow(DIM_ARRAY, 2),DIM_ARRAY))
        
        # diminui o tempo total do jogo (em um passo)
        self.game_length -= 1
        
        # confere se acabou o jogo
        if self.state[0] == self.pos_inimigo:
            reward = -1000
            done = True
        elif self.game_length <= 0:
            reward = 100
            done = True
        else:
            reward = 1
            done = False
        
        # cria um placeholder para a variável 'info' (não utilizada)
        info = {}
        
        # time.sleep(1)  # descomentar para deixar o jogo mais devagar
        
        # retorna as informações do step
        return self.state, reward, done, info

    def render(self, mode='human'):
        from gym.envs.classic_control import rendering
        
        box_width = VIEWPORT_W / SCALE
        box_height = VIEWPORT_H / SCALE

        if self.viewer is None:
            self.viewer = rendering.Viewer(int(VIEWPORT_W/2.6), int(VIEWPORT_H/2.6))
            
        for tile in range(0, (pow(DIM_ARRAY, 2))):
            a_t = int(tile / DIM_ARRAY)
            b_t = tile % DIM_ARRAY
            a_t += 1
            b_t += 1
            
            # if tile == 0 or tile == 8 or tile == 72 or tile == 80:
            #     jk=a_t*SCALE
            #     lm=b_t*SCALE
            #     print(f'tile({tile}), a({jk}), b({lm})')
            
            l, r, t, b = -box_width / 2, box_width / 2, box_height, 0
            
            if tile == self.state[0]:
                tile_geom = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
                tile_geom.set_color(0,0,0.8)
            elif tile == self.pos_inimigo:
                tile_geom = rendering.make_polygon([(l, b), (l, t), (r, t), (r, b)])
                tile_geom.set_color(0,0.8,0)
            else:
                tile_geom_fill = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
                tile_geom_fill.set_color(1,1,1)
                tile_geom_fill.add_attr(rendering.Transform(translation=(0, 0)))
                self.tile_geom_fill_trans = rendering.Transform()
                tile_geom_fill.add_attr(self.tile_geom_fill_trans)
                self.viewer.add_geom(tile_geom_fill)
                self.tile_geom_fill_trans.set_translation(a_t * SCALE, b_t * SCALE)
                
                tile_geom = rendering.make_polygon([(l, b), (l, t), (r, t), (r, b)], False)
                tile_geom.set_color(0,0,0)
            
            tile_geom.add_attr(rendering.Transform(translation=(0, 0)))
            self.tile_geom_trans = rendering.Transform()
            tile_geom.add_attr(self.tile_geom_trans)
            self.viewer.add_geom(tile_geom)
            self.tile_geom_trans.set_translation(a_t * SCALE, b_t * SCALE)
        
        # print(self.state[0])
        # print(self.where)
        # print(self.pos_inimigo)
        # print("")
        
        return self.viewer.render(return_rgb_array = mode=='rgb_array')
    
    def close(self):
        if self.viewer is not None:
            self.viewer.close()
            self.viewer = None
    
    def reset(self):
        # gera a posição aleatória da inimigo
        # inimigo
        where = random.choice([0, 1, 2, 3])
        '''
        where == 0: top
        where == 1: bottom
        where == 2: left
        where == 3: right
        '''
        if (where == 0):
            pos_inimigo_rand = random.choice(np.arange(0,DIM_ARRAY))
        if (where == 1):
            pos_inimigo_rand = random.choice(np.arange((pow(DIM_ARRAY, 2)-DIM_ARRAY),pow(DIM_ARRAY, 2)))
        if (where == 2):
            pos_inimigo_rand = random.choice(np.arange(0,(pow(DIM_ARRAY, 2)-(DIM_ARRAY-1)),DIM_ARRAY))
        if (where == 3):
            pos_inimigo_rand = random.choice(np.arange((DIM_ARRAY-1),pow(DIM_ARRAY, 2),DIM_ARRAY))
        
        # reatribui o estado inicial do player (sua posição)
        self.state = np.array([POS_PLAYER, pos_inimigo_rand]).astype(int)
        
        # reseta a posição da inimigo
        self.where = where
        self.pos_inimigo = pos_inimigo_rand
        
        # reseta o tempo total do jogo (em passos)
        self.game_length = GAME_LENGTH_TOTAL
        
        # print('Player: {}; Vaga: {};'.format(POS_PLAYER, pos_inimigo_rand))
        return self.state

# --------------------------- END CUSTOM ENV ----------------------------------------
