# pip install 'stable-baselines3[extra]' pyglet
import gym

# import helpers
import os

# import stable baselines
from stable_baselines3 import A2C
from stable_baselines3.common.vec_env import VecFrameStack

# import custom environment class
from customEnv import DodgeEnv

# TEST MODEL

NUM_EPISODES = 10

ALG_TYPE = 'A2C'
MODEL_NAME = '{}_Custom_Env_Dodge_Game'.format(ALG_TYPE)   # para LOAD e SAVE


log_path = os.path.join('Training', 'Logs')
model_Path = os.path.join('Training', 'Saved Models', MODEL_NAME)

env = DodgeEnv()

model = A2C.load(model_Path, env=env)
print('Loaded Model: {}\n'.format(MODEL_NAME))

episodes = NUM_EPISODES
for episode in range(1, episodes+1):
    obs = env.reset()
    done = False
    score = 0 
    
    while not done:
        env.render()
        action, _ = model.predict(obs)
        obs, reward, done, info = env.step(action)
        score+=reward
    print('\tEpisode: {}; Score: {};\n'.format(episode, score))

env.close()

