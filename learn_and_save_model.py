# pip install 'stable-baselines3[extra]' pyglet
import gym

# import helpers
import os

# import stable baselines
from stable_baselines3 import A2C
from stable_baselines3.common.vec_env import VecFrameStack

# import custom environment class
from customEnv import DodgeEnv

# LEARN

IS_LOAD = True  # mudar para falso caso seja o primeiro aprendizado e já não tenha um modelo prévio a carregar
# TOTAL_TIMESTEPS = 1000000
TOTAL_TIMESTEPS = 100000

ALG_TYPE = 'A2C'
MODEL_NAME = '{}_Custom_Env_Dodge_Game'.format(ALG_TYPE)   # para LOAD e SAVE


log_path = os.path.join('Training', 'Logs')
model_Path = os.path.join('Training', 'Saved Models', MODEL_NAME)

env = DodgeEnv()

if IS_LOAD:
    model = A2C.load(model_Path, env=env)
    print('Loaded Model: {}'.format(MODEL_NAME))
else:
    model = A2C('MlpPolicy', env, verbose=1, tensorboard_log=log_path)

model.learn(total_timesteps=TOTAL_TIMESTEPS)
model.save(model_Path)
