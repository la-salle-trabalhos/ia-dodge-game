# Dodge Game

Projeto de jogo para o trabalho de G2 da disciplina Inteligência Artificial I
da Universidade La Salle - Canoas, em 2022/01.

## Começando

Instruções para baixar e configurar esse projeto para executar localmente
na sua máquina local.

### Pré-Requisitos

Requisitos para a instalação, configuração e execução local do jogo:

- [Pip](https://pypi.org/project/pip/)
- [pyglet](https://pyglet.readthedocs.io/en/latest/index.html)
- [Gym](https://www.gymlibrary.ml) (suporta Python 3.7, 3.8, 3.9 and 3.10 no Linux e macOS, porém não suporta oficialmente no Windows)
- [Stable-Baselines3](https://stable-baselines3.readthedocs.io/en/master/) (necessita python 3.7+ e PyTorch >= 1.11)

### Instalação

Instalação do pip para Linux e MacOS (mais informações em <https://pip.pypa.io/en/stable/installation/>).

    python -m ensurepip --upgrade

Instalação do pip para Windows (mais informações em <https://pip.pypa.io/en/stable/installation/>).

    py -m ensurepip --upgrade

Instalação do pyglet (mais informações em <https://pyglet.readthedocs.io/en/latest/programming_guide/installation.html>):

    pip install pyglet

Instalação do gym (mais informações em <https://github.com/openai/gym>):

    pip install gym

Instalação do Stable-Baselines3 (mais informações em <https://stable-baselines3.readthedocs.io/en/master/guide/install.html>):

    pip install 'stable-baselines3[extra]'
