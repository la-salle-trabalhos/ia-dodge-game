# pip install 'stable-baselines3[extra]' pyglet
import gym

# import helpers
import os

# import stable baselines
from stable_baselines3 import A2C
from stable_baselines3.common.vec_env import VecFrameStack
from stable_baselines3.common.evaluation import evaluate_policy

# import custom environment class
from customEnv import DodgeEnv

# EVALUATE

IS_LOAD = True
N_EVAL_EPISODES = 100

ALG_TYPE = 'A2C'
MODEL_NAME = '{}_Custom_Env_Dodge_Game'.format(ALG_TYPE)   # para LOAD e SAVE


log_path = os.path.join('Training', 'Logs')
model_Path = os.path.join('Training', 'Saved Models', MODEL_NAME)

env = DodgeEnv()

if IS_LOAD:
    model = A2C.load(model_Path, env=env)
    print('Loaded Model: {}'.format(MODEL_NAME))
else:
    model = A2C('MlpPolicy', env, verbose=1, tensorboard_log=log_path)

mean_reward, std_reward = evaluate_policy(model, env, n_eval_episodes=N_EVAL_EPISODES, render=False)
print('Mean reward per episode: {}; Std of reward per episode: {};'.format(mean_reward, std_reward))
