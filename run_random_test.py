# pip install 'stable-baselines3[extra]' pyglet
import gym

# import helpers
import os

# import stable baselines
from stable_baselines3 import A2C
from stable_baselines3.common.vec_env import VecFrameStack

# import custom environment class
from customEnv import DodgeEnv

# RANDOM TEST

NUM_EPISODES = 5

log_path = os.path.join('Training', 'Logs')

env = DodgeEnv()
model = A2C('MlpPolicy', env, verbose=1, tensorboard_log=log_path)

episodes = NUM_EPISODES
for episode in range(1, episodes+1):
    state = env.reset()
    done = False
    score = 0
    
    while not done:
        env.render()
        action = model.action_space.sample()
        n_state, reward, done, info = env.step(action)
        score+=reward
    print('Episode: {}; Score: {};'.format(episode, score))

env.close()
